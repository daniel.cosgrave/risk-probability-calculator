function [percentAtkWin] = winProbEven(startingValue,finishingValue,numIterations)
    %
    
if nargin == 2
    numIterations = 1000;
end

if startingValue > finishingValue
    [finishingValue, startingValue] = deal(startingValue,finishingValue);
end

if(startingValue == 1)
     startingValue = startingValue + 1;
     disp("Number of Attackers cannot be 1, adding 1")
end


iter = 1;
winner = zeros(1,finishingValue-startingValue);
winval = zeros(1,finishingValue-startingValue);

for val = startingValue:finishingValue

    [winner(iter),winval(iter)]=Risk(val, val,numIterations);
    iter = iter + 1;
end

wins = strcmp('Defender',winner);
percentAtkWin = zeros(1,length(wins));

for iter = 1:length(wins)
    if wins(iter) == 1
        percentAtkWin = 1-winval;
    else
        percentAtkWin = winval;
    end
end

if nargout == 0
    plot(startingValue:finishingValue,percentAtkWin*100,'linewidth',2)
    title("Attacker Win Probability")
    xlabel("No. Atk & Def")
    ylabel("Probability of Attacker Winning (%)")
end

end