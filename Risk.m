function [winner,winval,proportion]=Risk(attacker, defender, numIterations)
%Risk calculates the odds for a single battle in the board game risk
%
% Risk(attacker, defender, numIterations)
% 
% INPUT
%        attacker: the integer number of attackers in the battle
%        defender: the number of defenders in the battle
%   numIterations: (opt.) the number of interations to test the battle on
%                  (default 1000)
%
% OUTPUT
%
% EXAMPLES
%     Risk(5,3)
%     Risk(12,12,1000)
%     Risk(10,20,10)
%
%
%
% Author:       Daniel Cosgrave
% email:        dcos54@gmail.com
% Matlab ver.:  R2020a
% Date:         21-Oct-2020
%-------------------------

if(attacker == 1)
    error("Attacker cannot be 1")
end

if(nargin == 2)
    numIterations = 1000;
end

attacker = abs(attacker);
defender = abs(defender);

maxAttack = 3;
maxDefense = 2;
attackerWins = 0;
defenderWins = 0;
winner = zeros(2);
outcome = [];
%figure
for iter = 1:numIterations
    tempAttacker = attacker;
    tempDefender = defender;
    while tempAttacker > 1 && tempDefender > 0
        if(tempAttacker <= maxAttack)
            attackerNumber = tempAttacker-1;
        else
            attackerNumber = maxAttack;
        end
        if(tempDefender < 2)
            defenderNumber = 1;
        else
            defenderNumber = maxDefense;
        end
        rolls = diceRolls(attackerNumber,defenderNumber);
        winner = (rolls(1,:) > rolls(2,:));
        numDefenderLoses = sum(winner);
        numAttackerLoses = length(winner) - sum(winner);    %length of winners - sum of non 1s
        tempAttacker = tempAttacker - numAttackerLoses;
        tempDefender = tempDefender - numDefenderLoses;
    end
    if tempAttacker == 1
        defenderWins = defenderWins + 1;
    end
    if tempDefender < 1
        attackerWins = attackerWins + 1;
    end
    
% 
%     clf('reset')    % clear current figure axes
%     hold on
%     labels = {'Attacker Wins: '; 'Defender Wins: '};
%     p = pie([attackerWins defenderWins],'%.2f%%');
%     axis off;
%     pText = findobj(p,'Type','text');
%     percentValues = get(pText,'String'); 
%     combinedtxt = strcat(labels,percentValues); 
%     title(['Number of wins in a ' num2str(attacker) ' vs ' num2str(defender) ' battle Iteration ' num2str(iter)])
%     drawnow
     outcome(iter, :) = [tempAttacker tempDefender];
    
end
%hold off
%close all
if nargout == 0
    %Pie chart of winners
    figure
    labels = {'Attacker Wins: '; 'Defender Wins: '};
    p = pie([attackerWins defenderWins],'%.2f%%');
    pText = findobj(p,'Type','text');
    percentValues = get(pText,'String'); 
    combinedtxt = strcat(labels,percentValues); 
    title(['Number of wins in a ' num2str(attacker) ' vs ' num2str(defender) ' battle'])
    %Change the labels by setting the String properties of the text objects to combinedtxt.

    pText(1).String = combinedtxt(1);
    pText(2).String = combinedtxt(2);

    %Histogram of remaining combatants
    axes1 = 0:max(attacker,defender);
    figure
    hist(outcome, axes1)
    xlabel('Number Remaining')
    ylabel('Number of iterations this occured')
    title(['Frequency of final outcomes in a ' num2str(attacker) ' vs ' num2str(defender) ' battle'])
    legend('Remaining Attackers', 'Remaining Defenders')
end
%Battle Outcomes

[bA, mA, nA] = unique(outcome(:,1));    %I don't know why but removing the first 2 results breaks this
[bB, mB, nB] = unique(outcome(:,2));
AVals = accumarray([nA(:), nB(:)],1);

%if(sum(D)==
D = zeros(1,defender);
A = zeros(1,attacker-1);
for iter = 1:length(AVals(1,:))-1
    D(1,iter) = AVals(1,iter+1);
end
for iter = 1:length(AVals(:,1))-1
    A(1,iter) = AVals(iter+1,1);
end
% for iter=1:length(AVals(:,1))
%     for jter = 1:length(AVals(1,:))
%         A(iter,jter) = AVals(iter,jter);
%     end
% end
defAxis = string(1:defender);
atkAxis = string(2:attacker);


if(defenderWins ~= 0)
    winval = attackerWins/(attackerWins + defenderWins);
    if winval < 0.5
        winner = "Defender";
        winval = 1-winval;
        [freq, num] = max(AVals(1,:));
        num = num-1;
        proportion = freq/sum(AVals(1,:));
    elseif winval == 0.5
        winner = "Unclear";
    else
        winner = "Attacker";
        [freq, num] = max((AVals(:,1)));
        proportion = freq/sum(AVals(:,1));
    end
else
    winner = "Attacker";
    winval = 1;
end

if nargout == 0
    if(strcmp(winner, "Unclear")~=0)
        disp('Median outcome: ' + winner + ' wins (' + winval*100 + '%) With ' + num + ' Remaining Troops: (' + proportion*100 + '%)') 
    else
        disp('Median outcome: ' + winner + ' wins (' + winval*100 + '%)')
    end

    figure
    subplot(2,1,1)
    h1 = heatmap(D);
    xlabel('Number of Remaining Defenders (when the defender wins)')
    title('Remaining victor combatants (frequency)')
    h1.XDisplayLabels = defAxis;
    grid off
    %ax.XData = split(num2str(0:defender),'  ');
    subplot(2,1,2)
    h2 = heatmap(A);
    xlabel('Number of Remaining Attackers (when the attacker wins)')
    h2.XDisplayLabels = atkAxis;
    colormap('bone')
    grid off
end

function rolls = diceRolls(numAttackers, numDefenders)
minRoll = 1;
maxRoll = 6;
combatants = min([numAttackers numDefenders]);
attackerRolls = sort(randi([minRoll maxRoll],1,numAttackers),'descend');
defenderRolls = sort(randi([minRoll maxRoll],1,numDefenders),'descend');
rolls(1,:) = attackerRolls(1:combatants);
rolls(2,:) = defenderRolls(1:combatants);